import handleRequest from "../../src/handle-request";

export const config = {
  runtime: "edge", 
  regions: [
    "cle1",
    "iad1",
    "pdx1",
    "sfo1",
    "sin1",
    "syd1",
    "hnd1",
    "kix1",
  ],
};

export default handleRequest;